#!/bin/bash

function gen_values {
	if [[ $# -ne 1 ]]
	then
		echo "$0: wrong number of arguments: $# instead of 1" >&2
		return 1
	fi

	array=()
	for ((i = 0; i < $1; ++i))
	do
		array+=( $RANDOM )
	done

	echo ${array[*]}
	return 0
}

function bubble {
	if [[ $# -ne 1 ]]
	then
		echo "$0: wrong number of arguments: $# instead of 1" >&2
		return 1
	fi

	array=($1)
	size=${#array[*]}

	for (( i = 0; i < $size; ++i ))
	do
		for (( j = 1; j < $(( $size - $i )); ++j ))
		do
			prev=$(( j - 1 ))
			if [[ ${array[j]} -lt ${array[prev]} ]]
			then
				tmp=${array[j]}
				array[j]=${array[prev]}
				array[prev]=$tmp
			fi
		done
	done

	echo ${array[*]}
	return 0
}

if [[ $# -eq 1 ]]
then
	num=$1
else
	echo "Recommended form: $0 <number>"
	read -p "Enter number: " num
fi

echo $(bubble "$(gen_values $num)")
