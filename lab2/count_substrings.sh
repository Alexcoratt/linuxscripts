#!/bin/bash

function count_substring {
	if [[ $# -ne 2 ]]
	then
		echo "Usage: $0 <substring> <string>"
		return 1
	fi

	echo "$2" | grep -o "$1" | wc -l
	return 1
}

if [[ $# -eq 2 ]]
then
	substr=$1
	str=$2
else
	echo "Recommended usage: $0 <substring> <string>"
	read -p "Enter the substring: " substr
	read -p "Enter the string: " str
fi

echo $(count_substring "$substr" "$str")
