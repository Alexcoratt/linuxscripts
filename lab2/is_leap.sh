#!/bin/bash

function is_leap {
	if [[ $# -ne 1 ]]
	then
		return 1
	fi

	if [[ $(( $1 % 400 )) -eq 0 ]]
	then
		echo 1
	elif [[ $(( $1 % 100 )) -eq 0 ]]
	then 
		echo 0
	elif [[ $(( $1 % 4 )) -eq 0 ]]
	then
		echo 1
	else
		echo 0
	fi
	
	return 0
}

if [[ $# -eq 1 ]]
then
	num=$1
else
	echo "Recommended form: $0 <year>"
	read -p "Enter year: " num
fi

echo -n "Year $num is "
res=$(is_leap $num)
if [[ $res -eq 1 ]]
then
	echo "leap"
else
	echo "not leap"
fi

