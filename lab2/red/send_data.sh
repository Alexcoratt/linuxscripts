#!/bin/bash

if [[ $# -ne 2 ]]
then
	echo "Usage: $0 <remote host> <remote directory>"
	return 1
else
	
	host=$1
	dir=$2

	# sending script results
	ncftpput -R $host $dir /scripts_res

	# sending backups
	ncftpput -R $host $dir /backup

	# sending server info
	ncftpput -R $host $dir /servinfo
fi
