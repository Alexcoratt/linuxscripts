#!/bin/bash

function write_info {
	if [[ $# -ne 3 ]]
	then
		echo "write_info: wrong argument number: 3 required, $# given"
		return 1
	fi

	log_fname=$1
	username=$2
	homedir=$3
	divider="======================="

	echo "username: $username" >> $log_fname
	echo "home directory: $homedir" >> $log_fname
	echo $divider >> $log_fname
	ls -la $homedir >> $log_fname 2>&1
	echo -e "$divider\n\n" >> $log_fname	
}

if [[ $# -ne 1 ]]
then
	info_dir=/servinfo
	echo "No directory to put information in specified"
	echo "Default directory selected: $info_dir"
else
	info_dir=$1
fi

if [[ -d $info_dir ]]
then
	# clearing directory
	rm -rf $info_dir/*
	
	# processes info
	ps -efww > $info_dir/processes.txt

	# connections info
	netstat -tupl > $info_dir/connections.txt

	# files and folders in users' directories
	user_info=$info_dir/user_info.txt
	echo "" > $user_info

	user_list=$info_dir/user_list.txt
	echo "" > $user_list

	IFS=$'\n'
	for line in $(getent passwd | awk -F: '{ printf("%s %s\n", $1, $6) }')
	do
		IFS=$' '
		pair=( $line )

		echo ${pair[0]} >> $user_list
		write_info $user_info ${pair[0]} ${pair[1]}
	
		if [[ $? -ne 0 ]]
		then
			break
		fi
		IFS=$'\n'
	done

	# network interfaces and addresses
	ip a > $info_dir/if_list.txt

	# routing table
	netstat -rn > $info_dir/routing_table.txt
	iptables-save > $info_dir/iptables_rules.v4.txt

else
	echo "directory named \"$info_dir\" does not exist"
fi


