#!/bin/bash

function oldest_dir {
	if [[ $# -ne 1 ]]
	then
		echo "Usage: $0 <directory>"
		return 1
	fi

	for name in $(ls -tr $1)
	do
		if [[ -d $1/$name ]]
		then
			echo $1/$name
			return 0
		fi
	done
	
	return 0
}

function dir_count {
	if [[ $# -ne 1 ]]
	then
		echo "Usage: $0 <directory>"
		return 1
	fi

	res=0
	for name in $1/*
	do
		if [[ -d $name ]]
		then
			res=$(( $res + 1 ))
		fi
	done
	
	echo $res
	return 0
}

if [[ $# -ne 3 ]]
then
	echo "Usage: $0 <backup destination directory> <backup limit> <backup source directory>"
else
	bkp_dest=$1
	limit=$2
	bkp_source=$3
	
	for (( i = $(dir_count $bkp_dest); i >= limit; --i ))
	do
		rm -r $(oldest_dir $bkp_dest)	
	done

	cp -r "$bkp_source" "$bkp_dest/$(date +%Y.%m.%d_%H:%M:%S.%N)_backup"
fi
