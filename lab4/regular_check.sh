#!/bin/bash

if [[ $# -ne 4 ]]
then
	echo "Usage: $0 <dirs to log> <dir to save logs> <diff report file name> <empty diff report file name>"
else
	# Logging constants
	ds=$1
	logdir=$2
	dr=$3
	edr=$4

	./create_report.sh "$ds" $logdir $edr > $dr

	# Mail constants
	from=alexsm.testmail@gmail.com
	login=$from
	password="pqvi kdee bgxj qqsg"
	to=aleqsonder@gmail.com
	smtp_server=smtp.gmail.com
	subject="WARNING: $(hostname): programs changed"

	if [[ $(du -b $edr | cut -f1) -lt $(du -b $dr | cut -f1) ]]
	then
		cat $dr | sendEmail -f $from -t $to -s $smtp_server -u $subject -o tls=yes -xu $login -xp "$password"
		echo "Message sent"
		echo "Report: $dr"
	else
		echo "No changes in $ds"
	fi
fi
