#!/bin/bash

if [[ $# -ne 3 ]]
then
	echo "Usage: $0 \"<dirs to log>\" <log dir> <empty report file>"
else
	dirs=$1
	logdir=$2
	empty_report=$3
	rm -f $empty_report
	for dir in $dirs
	do
		ldir=$logdir/$(echo "$dir" | tr / _)
		if [[ ! -d $ldir ]]
		then
			mkdir $ldir $ldir/logs
		fi

		./check_progs.sh $dir $ldir/logs 10 > $ldir/diffreport.txt

		echo "===== differences in $dir =====" | tee -a $empty_report
		cat $ldir/diffreport.txt
		echo -e "===== end $dir =====\n" | tee -a $empty_report
	done
fi
