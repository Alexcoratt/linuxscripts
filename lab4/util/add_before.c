#include <stdio.h>

int main(int argc, char **argv) {
	if (argc < 2) {
		fprintf(stderr, "Usage: %s <to add> [add to...]\n", argv[0]);
		return 1;
	}

	for (size_t i = 2; i < argc; ++i)
		printf("%s%s ", argv[1], argv[i]);
	printf("\n");
}
