#include <iostream>
#include <fstream>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <vector>
#include <algorithm>

std::vector<std::string> split(std::string const &line, std::string const & delim = " ") {
	auto dsize = delim.size();
	std::size_t partCount = 1;
	std::size_t lastPos = 0;
	for (std::size_t pos = 0; pos != std::string::npos; pos = line.find(delim, pos + dsize)) {
		if (pos - lastPos > dsize)
			++partCount;
		lastPos = pos;
	}

	std::vector<std::string> res(partCount);
	std::size_t index = 0;
	lastPos = 0;
	for (std::size_t pos = line.find(delim); lastPos != std::string::npos; pos = line.find(delim, pos + dsize)) {
		if (pos - lastPos > dsize)
			res[index++] = line.substr(lastPos, pos);
		lastPos = pos;
	}
	return res;
}

std::unordered_map<std::string, std::string> readFile(char const *fname, std::unordered_set<std::string> &keyset) {
	std::ifstream file(fname);
	if (!file.good()) {
		std::cerr << "Error occured while opening file named \"" << fname << "\"\n";
		file.close();
		return {};
	}

	std::unordered_map<std::string, std::string> res;
	std::string line;
	while (std::getline(file, line)) {
		auto parts = split(line);
		if (parts.size() != 2)
			std::cerr << "Invalid line \"" << line << "\"\n";
		else {
			res[parts.at(1)] = parts.at(0);
			keyset.insert(parts.at(1));
		}
	}

	return res;
}

int main(int argc, char **argv) {
	if (argc != 3 && argc != 4) {
		std::cerr << "Usage: " << argv[0] << " <first file> <second file> [output file]\n";
		return 1;
	}

	std::unordered_set<std::string> files;

	auto oldSums = readFile(argv[1], files);
	auto newSums = readFile(argv[2], files);

	std::ostream *out = &std::cout;
	if (argc == 4) {
		std::ofstream *o = new std::ofstream(argv[3]);
		if (!o->good()) {
			std::cerr << "Error occured while opening file named \"" << argv[3] << "\"\n";
			o->close();
			return 1;
		}
	}

	std::vector<std::string> f_ord(files.begin(), files.end());
	std::sort(f_ord.begin(), f_ord.end());

	for (auto const &file : f_ord) {
		auto oldIter = oldSums.find(file);
		auto newIter = newSums.find(file);

		if (oldIter != oldSums.end()) {
			if (newIter == newSums.end())
				*out << "Deleted:\t" << oldIter->first << "\t" << oldIter->second << '\n';
			else if (oldIter->second != newIter->second)
				*out << "Modified:\t" << oldIter->first << "\t" << oldIter->second << " >>> " << newIter->second << '\n';
		} else if (newIter != newSums.end())
			*out << "New file:\t" << newIter->first << "\t" << newIter->second << '\n';
	}

	return 0;
}
