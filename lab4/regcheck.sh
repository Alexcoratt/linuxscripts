#!/bin/bash

if [[ $# -ne 1 ]]
then
	echo "Usage: $0 <regular_check dir> "
else
	cd $1
	./regular_check.sh "/bin /usr/bin" /difflog /difflog/diff_report.txt /difflog/empty_diff_report.txt > /difflog/last_responce.txt 2>&1 
fi
