#!/bin/bash

# This script should be executed under the superuser

function make_log {
	if [[ $# -ne 1 ]]
	then
		echo "Usage: make_log <directory>"
		return 1
	fi
	
	for entry in $1/*
	do
		if [[ -f $entry ]]
		then
			md5sum $entry
		fi
	done
}

if [[ $# -ne 3 ]]
then
	echo "Usage: $0 <dir to log> <log dir> <max log files>"
else
	dir=$1
	logdir=$2
	maxlogs=$3
	if [[ -d $dir ]]
	then
		if [[ -d $logdir ]]
		then	
			rm -f $(./util/add_before "$logdir/" $(ls -t $logdir | tail -n +$maxlogs))
		else
			mkdir $logdir
		fi
		make_log $dir > $logdir/log_$(date +%Y.%m.%d_%H:%M:%S.%N).txt
	fi

	if [[ $(ls $logdir | wc -l) -ge 2 ]]
	then
		./util/diff $(./util/add_before "$logdir/" $(ls -tr $logdir | tail -n 2))
	fi
fi

