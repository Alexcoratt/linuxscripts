#!/bin/bash

# This script has to be executed under the root user

if [[ $# -ne 5 ]]
then
	echo "Usage: $0 <username> <password> <homedir> <output file name> <port>"
else
	# Installing required packages
	apt install gcc ncat

	# Setting variables
	export USERNAME=$1
	export PASSWORD=$2
	export HOMEDIR=$3
	export OFILENAME=$4
	export PORT=$5

	# Adding user
	useradd $USERNAME -m -d $HOMEDIR -p $PASSWORD

	# Compilling special utility
	gcc read_interpret.c -o ri

	# Copying service file to systemd
	./ri -i nc.service.template -o /etc/systemd/system/nc.service

	# Starting service
	systemctl daemon-reload
	systemctl enable nc.service
	systemctl start nc.service

	# Deleting compilled utility
	rm ri
fi
