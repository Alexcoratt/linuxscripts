#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define DEFAULT_BUFFER_SIZE 17

char * read_name(FILE * ifptr, char * endptr, size_t default_buffer_size) {
	size_t buffer_size = default_buffer_size;
	char *buffer = calloc(buffer_size, sizeof(char));
			
	size_t i = 0;
	char symb;
	while ((symb = fgetc(ifptr)) != EOF && (isalpha(symb) || isdigit(symb) || symb == '_')) {
		if (i >= buffer_size) {
			char *tmp = calloc(buffer_size * 2, sizeof(char));
			memcpy(tmp, buffer, buffer_size);
			free(buffer);
			buffer = tmp;
			buffer_size *= 2;
		}
		buffer[i++] = symb;
	}

	if (endptr)
		*endptr = symb;
	return buffer;
}

#define WRONG_USAGE_OF_FLAG "Wrong usage of flag \"-%c\"\n"
#define FILE_ERROR "Error occured while opening file \"%s\"\n"
#define UNKNOWN_FLAG "Unknown flag \"-%c\"\n"

int get_io(int argc, char ** argv, FILE ** ifptr, FILE ** ofptr) {
	FILE * in = stdin;
	FILE * out = stdout;
	for (int i = 1; i < argc; ++i) {
		if (argv[i][0] == '-') {
			switch (argv[i][1]) {
				case 'i':
					if (i + 1 >= argc) {
						fprintf(stderr, WRONG_USAGE_OF_FLAG, argv[i][1]);
						return 1;
					}
					in = fopen(argv[i + 1], "r");
					if (!in) {
						fprintf(stderr, FILE_ERROR, argv[i + 1]);
						return 1;
					}
					break;
				case 'o':
					if (i + 1 >= argc) { 
						fprintf(stderr, WRONG_USAGE_OF_FLAG, argv[i][1]);
						return 1;
					}
					out = fopen(argv[i + 1], "w");
					if (!out) {
						fprintf(stderr, FILE_ERROR, argv[i + 1]);
						return 1;
					}
					break;
				default:
					fprintf(stderr, UNKNOWN_FLAG, argv[i][1]);
					return 1;
			}
		}
	}

	if (ifptr)
		*ifptr = in;
	if (ofptr)
		*ofptr = out;
	return 0;
}

int main(int argc, char ** argv) {
	FILE * ifptr;
	FILE * ofptr;

	if (get_io(argc, argv, &ifptr, &ofptr)) {
		fprintf(stderr, "Usage: %s [-i input filename] [-o output filename]\n", argv[0]);
		return 1;
	}

	char symb = 0;
	char prev_symb = 0;

	while ((symb = fgetc(ifptr)) != EOF) {
		if (prev_symb == '\\') {
			switch (symb) {
				case '\\':
					break;
				case '$':
					symb = '$';
					break;
				case 'n':
					symb = '\n';
					break;
				case 't':
					symb = '\t';
					break;
				default:
					fprintf(stderr, "Unknown sequence: \'\\%c\'\n", symb);
					return 1;
			}
			putc(symb, ofptr);
			symb = 0;
		} else {
			if (symb == '$') {
				char endsymb;
				char * varname = read_name(ifptr, &endsymb, DEFAULT_BUFFER_SIZE);
				fprintf(ofptr, "%s%c", getenv(varname), endsymb);
				free(varname);
			} else if (symb != '\\') {
				putc(symb, ofptr);
			}
		}
		prev_symb = symb;
	}

	fclose(ifptr);
	fclose(ofptr);

	return 0;
}
